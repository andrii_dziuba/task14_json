import com.fasterxml.jackson.databind.ObjectMapper;
import comparators.BankComparator;
import model.Bank;
import parser.JsonHandler;
import validator.JsonValidator;

import java.io.*;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Scanner;

public class Application {

    static File jsonFile = new File("banks.json");

    public static void main(String[] args) {
        JsonHandler jsonHandler = new JsonHandler();
        List<Bank> banks = null;

        String schema = null;
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream fis = new BufferedInputStream(new FileInputStream(jsonFile));
            try (Scanner sc = new Scanner(fis)) {
                while(sc.hasNext()) {
                    stringBuilder.append(sc.nextLine());
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        schema = stringBuilder.toString();

        JsonValidator jsonValidator = new JsonValidator();
        boolean validate = jsonValidator.validate("jsonSchema.json", schema);
        if(validate) {
            System.out.println("Json matches the schema.");
        } else {
            System.out.println("Json is not match the schema.");
        }

        try {
            banks = jsonHandler.readJsonFile(jsonFile);
        } catch (IOException e) {
            e.printStackTrace();
        }



        System.out.println("Sorted by ID:");
        printSorted(banks, BankComparator.SORTERS.ID);
        System.out.println("Sorted by name:");
        printSorted(banks, BankComparator.SORTERS.NAME);
        System.out.println("Sorted by country:");
        printSorted(banks, BankComparator.SORTERS.COUNTRY);
        System.out.println("Sorted by count of deposits:");
        printSorted(banks, BankComparator.SORTERS.COUNT_OF_DEPOSITS);

    }

    static void printSorted(List<Bank> banks, BankComparator.SORTERS sortBy) {
        banks.stream().sorted(new BankComparator(sortBy)).forEach(System.out::println);
    }
}
