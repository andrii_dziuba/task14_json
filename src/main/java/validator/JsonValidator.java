package validator;

import org.everit.json.schema.Schema;
import org.everit.json.schema.ValidationException;
import org.everit.json.schema.loader.SchemaLoader;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class JsonValidator {

    public boolean validate(String schemaPath, String json) {
        try (InputStream inputStream = new FileInputStream(schemaPath)) {
            JSONObject rawSchema = new JSONObject(new JSONTokener(inputStream));
            Schema schema = SchemaLoader.load(rawSchema);
            try {
                schema.validate(new JSONArray(json));
            } catch (ValidationException ee) {
                System.err.println(ee.getMessage());
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return true;
    }

}
