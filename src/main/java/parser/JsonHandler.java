package parser;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import model.Bank;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class JsonHandler {
    ObjectMapper objectMapper = new ObjectMapper();

    public <T> T readJsonFile(File file) throws IOException {
        T o = objectMapper
                .readValue(new FileInputStream(file), new TypeReference<ArrayList<Bank>>(){});
        return o;
    }
}
