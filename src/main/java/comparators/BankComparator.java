package comparators;

import com.sun.istack.internal.NotNull;
import model.Bank;

import java.util.Comparator;

public class BankComparator implements Comparator<Bank> {

    private final SORTERS sortBy;

    public enum SORTERS {
        NAME, COUNTRY, COUNT_OF_DEPOSITS, ID;
    }

    public BankComparator(@NotNull SORTERS sortBy) {
        this.sortBy = sortBy;
    }

    @Override
    public int compare(Bank o1, Bank o2) {
        switch (sortBy) {
            case ID:
                return o1.getId()-o2.getId();
            case NAME:
                return o1.getName().compareToIgnoreCase(o2.getName());
            case COUNTRY:
                return o1.getCountry().compareToIgnoreCase(o2.getCountry());
            case COUNT_OF_DEPOSITS:
                return o1.getActiveDeposits().size()-o2.getActiveDeposits().size();
                default:
                    return 0;
        }
    }
}
